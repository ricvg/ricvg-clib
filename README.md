# RicVG's Clib #

Presently this Clib contains only the implementation for a circular queue that uses the [uTest](https://bitbucket.org/ricvg/utest) library.

Clib is used to demonstrate how to use uTest. Take a look at [test-src/clib-test.c](https://bitbucket.org/ricvg/ricvg-clib/src/f86850d24f6fb01009f86feb976eb9d4d8f0910b/test-src/clib-test.c?at=master) for an example.