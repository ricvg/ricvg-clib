#include "../src/rvglib.h"
// ............................................................................
DEFINE_THIS_FILE;
// ............................................................................
#define EBUFF_PTR(CQPTR) &((CQPTR)->elementBuffer)
#define SET_START(CQPTR, START) *((size_t*)&(CQPTR)->start) = START
#define SET_END(CQPTR, END) *((size_t*)&(CQPTR)->end) = END
#define SET_SIZE(CQPTR, SIZE) *((size_t*)&(CQPTR)->size) = SIZE
// ............................................................................
static void increment_end(struct VCircularQueue const* cqueue) {
	SET_END(cqueue, cqueue->end+1);
	if(cqueue->end == vElementBuffer_ElementCapacity(EBUFF_PTR(cqueue))) {
		SET_END(cqueue, 0);
	}
}
// ............................................................................
static void increment_start(struct VCircularQueue const* cqueue) {
	SET_START(cqueue, cqueue->start+1);
	if(cqueue->start == vElementBuffer_ElementCapacity(EBUFF_PTR(cqueue))) {
		SET_START(cqueue, 0);
	}
}
// ............................................................................
void VCircularQueue_Ctor(struct VCircularQueue const* cqueue,
                            uint8_t const* buffer,
                            const size_t buffer_size,
                            const size_t elem_size)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	vElementBuffer_Ctor(EBUFF_PTR(cqueue), buffer, buffer_size, elem_size);
	SET_START(cqueue, 0);
	SET_END(cqueue, 0);
	SET_SIZE(cqueue, 0);
}
// ............................................................................
void VCircularQueue_Xtor(struct VCircularQueue const* cqueue)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
}
// ............................................................................
void VCircularQueue_Init(struct VCircularQueue const* cqueue)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
}
// ............................................................................
size_t VCircularQueue_Size(struct VCircularQueue const* cqueue)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	return cqueue->size;
}
// ............................................................................
size_t VCircularQueue_Capacity(struct VCircularQueue const* cqueue)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	return vElementBuffer_ElementCapacity(EBUFF_PTR(cqueue));
}
// ............................................................................
size_t VCircularQueue_IsEmpty(struct VCircularQueue const* cqueue)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	return cqueue->size == 0;
}
// ............................................................................
size_t VCircularQueue_IsFull(struct VCircularQueue const* cqueue)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	return (cqueue->size != 0) && (cqueue->start == cqueue->end);
}
// ............................................................................
void VCircularQueue_Push(struct VCircularQueue const* cqueue, void* element)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	ASSERT(element != NULL, ZCQUEUE_ERROR_ELEMENT_NULL_PTR);
	ASSERT(0 == VCircularQueue_IsFull(cqueue), ZCQUEUE_ERROR_PUSH_NO_SPACE_LEFT);
	vElementBuffer_SetElement(EBUFF_PTR(cqueue), cqueue->end, element);
	increment_end(cqueue);
	SET_SIZE(cqueue, cqueue->size+1);
}
// ............................................................................
void VCircularQueue_Pop(struct VCircularQueue const* cqueue, void* element, const size_t element_buffer_size)
{
	ASSERT(cqueue != NULL, ZCQUEUE_ERROR_NULL_PTR);
	ASSERT(element != NULL, ZCQUEUE_ERROR_ELEMENT_NULL_PTR);
	ASSERT(0 == VCircularQueue_IsEmpty(cqueue), ZCQUEUE_ERROR_POP_BUFFER_EMPTY);
	vElementBuffer_GetElement(EBUFF_PTR(cqueue), cqueue->start, element, element_buffer_size);
	increment_start(cqueue);
	SET_SIZE(cqueue, cqueue->size-1);
}
// ............................................................................
