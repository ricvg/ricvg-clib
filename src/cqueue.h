/*
 * Copyright 2015 - Ricardo Villalobos Guevara
 */

#ifndef CQUEUE_H_
#define CQUEUE_H_
// ............................................................................
enum VCQUEUE_ERROR {
	ZCQUEUE_ERROR_NULL_PTR = LAST_ERROR,
	ZCQUEUE_ERROR_ELEMENT_NULL_PTR,
	ZCQUEUE_ERROR_PUSH_NO_SPACE_LEFT,
	ZCQUEUE_ERROR_POP_BUFFER_EMPTY,
};
// ............................................................................
struct VCircularQueue {
	const struct ElementBuffer elementBuffer;
	const size_t start;
	const size_t end;	/// Index of next available slot
	const size_t size;
};
// ............................................................................
void VCircularQueue_Ctor(struct VCircularQueue const* cqueue, uint8_t const* buffer, const size_t buffer_size, const size_t element_size);
void VCircularQueue_Xtor(struct VCircularQueue const* cqueue);
void VCircularQueue_Init(struct VCircularQueue const* cqueue);
size_t VCircularQueue_Size(struct VCircularQueue const* cqueue);
size_t VCircularQueue_Capacity(struct VCircularQueue const* cqueue);
void VCircularQueue_Push(struct VCircularQueue const* cqueue, void* element);
void VCircularQueue_Pop(struct VCircularQueue const* cqueue, void* element, const size_t element_buffer_size);
size_t VCircularQueue_IsEmpty(struct VCircularQueue const* cqueue);
size_t VCircularQueue_IsFull(struct VCircularQueue const* cqueue);
// ............................................................................
#endif /* CQUEUE_H_ */
