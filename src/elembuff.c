/*
 * Copyright 2015 - Ricardo Villalobos Guevara
 */
#include <string.h>

#include "../src/rvglib.h"
// ............................................................................
DEFINE_THIS_FILE;
// ............................................................................
#define SET_BUFFER_PTR(EBUFF_PTR, BUFFER_PTR) *(uint8_t const**)&((EBUFF_PTR)->buffer) = (BUFFER_PTR)
#define SET_BUFFER_SIZE(EBUFF_PTR, BUFFER_SIZE) *(size_t *)& (EBUFF_PTR)->size = (BUFFER_SIZE)
#define SET_ELEMENT_SIZE(EBUFF_PTR, ELEM_SIZE) *(size_t *)& (EBUFF_PTR)->elem_size = (ELEM_SIZE)
#define ELEMENTS_CAPACITY(EBUFF_PTR) ((EBUFF_PTR)->size / (EBUFF_PTR)->elem_size)
// ............................................................................
void vElementBuffer_Ctor(const struct ElementBuffer const * elembuffer,
                  uint8_t  const * buffer_ptr,
                  const size_t buffer_size,
                  const size_t elem_size)
{
	ASSERT(elembuffer != 0, EBUFF_ERROR_NULL_PTR);
	ASSERT(buffer_ptr != 0, EBUFF_ERROR_BUFFER_NULL_PTR);
	ASSERT(buffer_size > 0, EBUFF_ERROR_BUFFER_SIZE_IS_ZERO);
	ASSERT(elem_size > 0, EBUFF_ERROR_ELEMENT_SIZE_IS_ZERO);
	ASSERT(buffer_size >= elem_size, EBUFF_ERROR_BUFFER_TOO_SMALL_FOR_ELEMENT);
	ASSERT((buffer_size % elem_size) == 0, EBUFF_ERROR_BUFFER_SIZE_NOT_A_MULTIPLE_OF_ELEMENT_SIZE);
	SET_BUFFER_PTR(elembuffer, buffer_ptr);
	SET_BUFFER_SIZE(elembuffer, buffer_size);
	SET_ELEMENT_SIZE(elembuffer, elem_size);
}
// ............................................................................
void vElementBuffer_Xtor(struct ElementBuffer const * elembuffer)
{
}
// ............................................................................
void vElementBuffer_Init(struct ElementBuffer const * elembuffer)
{
}
// ............................................................................
void vElementBuffer_SetElement(struct ElementBuffer const * elembuffer,
                         const uint16_t index,
                         const void const* element)
{
	ASSERT(elembuffer != 0, EBUFF_ERROR_NULL_PTR);
	ASSERT(element != 0, EBUFF_ERROR_ELEMENT_NULL_PTR);
	ASSERT(index < ELEMENTS_CAPACITY(elembuffer), EBUFF_ERROR_INDEX_OUT_OF_BOUNDS);
	uint8_t const * ptr = ((uint8_t const *)elembuffer->buffer) + (index * elembuffer->elem_size);
	memcpy((void*)ptr, element, elembuffer->elem_size);
}
// ............................................................................
void vElementBuffer_GetElement(struct ElementBuffer const * elembuffer,
		                 const uint16_t index,
                         void const* target_buffer_ptr,
                         const size_t target_buffer_size)
{
	ASSERT(elembuffer != 0, EBUFF_ERROR_NULL_PTR);
	ASSERT(target_buffer_ptr != 0, EBUFF_ERROR_ELEMENT_NULL_PTR);
	ASSERT(target_buffer_size >= elembuffer->elem_size, EBUFF_ERROR_INSUFFICIENT_BUFFER_SIZE);
	ASSERT(index < ELEMENTS_CAPACITY(elembuffer), EBUFF_ERROR_INDEX_OUT_OF_BOUNDS);
	uint8_t const * ptr = ((uint8_t const *)elembuffer->buffer) + (index * elembuffer->elem_size);
	memcpy((void*)target_buffer_ptr, (void*)ptr, elembuffer->elem_size);
}
// ............................................................................
size_t vElementBuffer_ElementCapacity(const struct ElementBuffer const * elembuffer)
{
	ASSERT(elembuffer != 0, EBUFF_ERROR_NULL_PTR);
	return ELEMENTS_CAPACITY(elembuffer);
}
