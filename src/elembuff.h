/*
 * Copyright 2015 - Ricardo Villalobos Guevara
 */

#ifndef ELEMBUFF_H_
#define ELEMBUFF_H_
// ............................................................................
#ifdef __cplusplus
extern "C" {
#endif
// ............................................................................
struct ElementBuffer {
	const uint8_t const * buffer;
	const size_t size;
	const size_t elem_size;
};
// ............................................................................
enum EBUFF_ERROR {
	EBUFF_ERROR_NULL_PTR = LAST_ERROR,
	EBUFF_ERROR_BUFFER_NULL_PTR,
	EBUFF_ERROR_ELEMENT_NULL_PTR,
	EBUFF_ERROR_BUFFER_SIZE_IS_ZERO,
	EBUFF_ERROR_ELEMENT_SIZE_IS_ZERO,
	EBUFF_ERROR_BUFFER_TOO_SMALL_FOR_ELEMENT,
	EBUFF_ERROR_BUFFER_SIZE_NOT_A_MULTIPLE_OF_ELEMENT_SIZE,
	EBUFF_ERROR_INDEX_OUT_OF_BOUNDS,
	EBUFF_ERROR_INSUFFICIENT_BUFFER_SIZE,
	EBUFF_ERROR_LAST
};
// ............................................................................
/** Constructor which assembles the ElementBuffer type.
 */
void vElementBuffer_Ctor(struct ElementBuffer const * elembuffer,    ///< [in,out] Constructed ElementBuffer
                                          uint8_t const * buffer,        ///< [in] Pointer to buffer for this object
                                           const size_t   buffer_size,   ///< [in] Size of buffer in bytes
                                           const size_t   element_size); ///< [in] Element size in bytes
// ............................................................................
/** Destroys the ElementBuffer type.
 *
 * The behaviour of an object after is has been destroyed is undefined.
 */
void vElementBuffer_Xtor(struct ElementBuffer const * elembuffer);    ///< [in,out] Destroyed ElementBuffer
// ............................................................................
/** Initializes the ElementBuffer type.
 */
void vElementBuffer_Init(struct ElementBuffer const * elembuffer);    ///< [in,out] Initialized ElementBuffer
// ............................................................................
/** Copy element data to ElementBuffer at specified index.
 */
void vElementBuffer_SetElement(struct ElementBuffer const * elembuffer, ///< [in,out] ElementBuffer
                                                const uint16_t   index,      ///< [in] Element position inside buffer
                                             const void const * element);   ///< [in] Pointer to element data to be copied
// ............................................................................
/** Copy element data from ElementBuffer at specified index to provided buffer.
 */
void vElementBuffer_GetElement(struct ElementBuffer const * elembuffer,          ///< [in] ElementBuffer
                                                const uint16_t   index,               ///< [in] Element position inside buffer
                                                   void const * target_buffer_ptr,   ///< [in,out] Pointer to buffer to copy data to
                                                  const size_t   target_buffer_size); ///< [in] Size in bytes of buffer to receive data
// ............................................................................
/** Capacity of elements in ElementBuffer.
 */
size_t vElementBuffer_ElementCapacity(const struct ElementBuffer const * elembuffer);
// ............................................................................
#ifdef __cplusplus
}
#endif
// ............................................................................
#endif /* ELEMBUFF_H_ */
