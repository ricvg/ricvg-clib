/** \file
 * The code for this file was obtained in part from the file qassert.h found
 * in page 221 of the book "Practical Statecharts in C/C++."
 *
 * Original copyright:
 *
 * Quantum Assertions--Design by Contract(TM of ISI) facilities
 * Copyright (c) 2002 Miro Samek, Palo Alto, CA.
 * All Rights Reserved.
 */
#ifndef __RASSERT_H
#define __RASSERT_H
// ............................................................................
#ifdef __cplusplus
extern "C" {
#endif
// ............................................................................
#ifndef NASSERT
// ............................................................................
/**
 * Macro instantiates a single string with the file name.
 */
#  define DEFINE_THIS_FILE static const char THIS_FILE__[] = __FILE__
// ............................................................................
/**
 * Project specific function called by assertion macros.
 *
 * @param[in] file is a string with the filename where the assertion was generated.
 * @param[in] line is the line number where the assertion was generated.
 * @param[in] error is an arbritary error code..
 */
extern void on_assert__(const char* file, unsigned line, int error);
// ............................................................................
/**
 * Invokes on_assert__() if the input test fails.
 *
 * @param[in] test_ is a statement to be tested.
 * @param[in] error is an arbritary code.
 */
#  define ASSERT(test_, error)                                  \
               if (test_) {                                     \
               }                                                \
               else                                             \
                   on_assert__(THIS_FILE__, __LINE__, error)
// ............................................................................
#  define ALLEGE(test_, error)        ASSERT(test_, error)
#  define REQUIRE(test_, error)       ASSERT(test_, error)
#  define REQUIRE_FALSE(test_, error) ASSERT(0 == (test_), error)
#  define REQUIRE_TRUE(test_, error)  ASSERT(0 != (test_), error)
// ............................................................................
#else // NASSERT
#  define DEFINE_THIS_FILE extern const char THIS_FILE__[]
#  define ASSERT(test_, error)
#  define ENSURE(test_, error)
#  define REQUIRE(test_, error)
#  define REQUIRE_FALSE(test_, error)
#  define REQUIRE_TRUE(test_, error)
#  define INVARIANT(test_, error)
#  define ALLEGE(test_, error) 	\
              if (test_) {          \
	          }                     \
	          else

#endif // NASSERT
#ifdef __cplusplus
}
#endif
/** \def ALLEGE
 * Allows testing of expressions with side effects.
 *
 * @param[in] test_ is an expression to be tested with possible side effects.
 * @param[in] error Error code passed to the assert function.
 */
#endif /* __RASSERT_H */
