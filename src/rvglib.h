/*
 * Copyright 2015 - Ricardo Villalobos Guevara
 */
#ifndef RVGLIB_H_
#define RVGLIB_H_

#include <stdlib.h>
#include <stdint.h>
#include "rassert.h"

#define LAST_ERROR 0
#include "elembuff.h"
#undef LAST_ERROR
#define LAST_ERROR EBUFF_ERROR_LAST
#include "cqueue.h"

#endif /* RVGLIB_H_ */
