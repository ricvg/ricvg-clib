/*
 * Copyright 2012 Ricardo Villalobos Guevara
 */
#include <stdio.h>
#include <stdlib.h>

#include "../src/rvglib.h"
#include "utest/utest.h"
//............................................................................
typedef uint16_t Element;
const uint8_t element_size = sizeof(Element);
#define ELEMENT_NUMBER 5
uint8_t buffer[ELEMENT_NUMBER * sizeof(Element)];
//............................................................................
struct ElementBuffer elembuff_instance;
struct ElementBuffer* elembuff = &elembuff_instance;
//............................................................................
int test_lembuff_ctor_invalid() {
	EXPECT_ASSERTION(vElementBuffer_Ctor(0, 0, 0, 0), EBUFF_ERROR_NULL_PTR);
	EXPECT_ASSERTION(vElementBuffer_Ctor(elembuff, 0, 0, 0), EBUFF_ERROR_BUFFER_NULL_PTR);
	EXPECT_ASSERTION(vElementBuffer_Ctor(elembuff, buffer, 0, 0), EBUFF_ERROR_BUFFER_SIZE_IS_ZERO);
	EXPECT_ASSERTION(vElementBuffer_Ctor(elembuff, buffer, 1, 0), EBUFF_ERROR_ELEMENT_SIZE_IS_ZERO);
	EXPECT_ASSERTION(vElementBuffer_Ctor(elembuff, buffer, sizeof(buffer), element_size+1), EBUFF_ERROR_BUFFER_SIZE_NOT_A_MULTIPLE_OF_ELEMENT_SIZE);
	return 0;
}
//............................................................................
int test_lembuff_set_invalid() {
	EXPECT_NO_ASSERTIONS(vElementBuffer_Ctor(elembuff, buffer, sizeof(buffer), element_size));
	EXPECT_ASSERTION(vElementBuffer_SetElement(0, 0, 0), EBUFF_ERROR_NULL_PTR);
	EXPECT_ASSERTION(vElementBuffer_SetElement(elembuff, 0, 0), EBUFF_ERROR_ELEMENT_NULL_PTR);
	Element element = 0;
	uint8_t invalid_index = 5;
	EXPECT_ASSERTION(vElementBuffer_SetElement(elembuff, invalid_index, &element), EBUFF_ERROR_INDEX_OUT_OF_BOUNDS);
	return 0;
}
//............................................................................
int test_lembuff_get_invalid() {
	EXPECT_NO_ASSERTIONS(vElementBuffer_Ctor(elembuff, buffer, sizeof(buffer), element_size));
	Element element1 = 0xAA;
	EXPECT_NO_ASSERTIONS(vElementBuffer_SetElement(elembuff, 0, &element1));
	TEST_REQUIRE(buffer[0] == element1);
	Element element2 = 0;
	EXPECT_ASSERTION(vElementBuffer_GetElement(elembuff, 0, &element2, sizeof(element2)), EBUFF_ERROR_NULL_PTR);
	EXPECT_ASSERTION(vElementBuffer_GetElement(elembuff, 0, 0, sizeof(element2)), EBUFF_ERROR_ELEMENT_NULL_PTR);
	uint8_t invalid_element_size = 0;
	EXPECT_ASSERTION(vElementBuffer_GetElement(elembuff, 0, &element2, invalid_element_size), EBUFF_ERROR_INDEX_OUT_OF_BOUNDS);
	uint8_t invalid_index = 10;
	EXPECT_ASSERTION(vElementBuffer_GetElement(elembuff, invalid_index, &element2, sizeof(element2)), EBUFF_ERROR_INDEX_OUT_OF_BOUNDS);
	return 0;
}
//............................................................................
int test_lembuff_valid() {

	EXPECT_NO_ASSERTIONS(vElementBuffer_Ctor(elembuff, buffer, sizeof(buffer), element_size));
	TEST_REQUIRE(elembuff->buffer == buffer);
	TEST_REQUIRE(elembuff->size == sizeof(buffer));
	TEST_REQUIRE(elembuff->elem_size == element_size);
	Element element1 = 0xAA;
	EXPECT_NO_ASSERTIONS(vElementBuffer_SetElement(elembuff, 0, &element1));
	TEST_REQUIRE(buffer[0] == element1);
	Element element2 = 0;
	EXPECT_NO_ASSERTIONS(vElementBuffer_GetElement(elembuff, 0, &element2, sizeof(element2)));
	TEST_REQUIRE(element2 == element1);
	size_t capacity;
	EXPECT_NO_ASSERTIONS(capacity = vElementBuffer_ElementCapacity(elembuff));
	TEST_REQUIRE(capacity == sizeof(buffer) / element_size);
	return 0;
}
//............................................................................
struct VCircularQueue cqueue_instance;
struct VCircularQueue const * cqueue = &cqueue_instance;
//............................................................................
int test_vcqueue_valid() {
	EXPECT_ASSERTION(VCircularQueue_Ctor(0, buffer, sizeof(buffer), element_size), ZCQUEUE_ERROR_NULL_PTR);
	EXPECT_NO_ASSERTIONS(VCircularQueue_Ctor(cqueue, buffer, sizeof(buffer), element_size));
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(ELEMENT_NUMBER == VCircularQueue_Capacity(cqueue));
	size_t count;
	EXPECT_NO_ASSERTIONS(count = VCircularQueue_Size(cqueue));
	TEST_REQUIRE(count == 0);
	Element element1 = 0xAA;
	EXPECT_NO_ASSERTIONS(VCircularQueue_Push(cqueue, &element1));
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(cqueue->end == 1);
	TEST_REQUIRE(1 == VCircularQueue_Size(cqueue));
	Element element2 = 0xBB;
	EXPECT_NO_ASSERTIONS(VCircularQueue_Push(cqueue, &element2));
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(cqueue->end == 2);
	TEST_REQUIRE(2 == VCircularQueue_Size(cqueue));
	Element element3 = 0xCC;
	EXPECT_NO_ASSERTIONS(VCircularQueue_Push(cqueue, &element3));
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(cqueue->end == 3);
	TEST_REQUIRE(3 == VCircularQueue_Size(cqueue));
	Element element4 = 0xDD;
	EXPECT_NO_ASSERTIONS(VCircularQueue_Push(cqueue, &element4));
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(cqueue->end == 4);
	TEST_REQUIRE(4 == VCircularQueue_Size(cqueue));
	Element element5 = 0xEE;
	EXPECT_NO_ASSERTIONS(VCircularQueue_Push(cqueue, &element5));
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(5 == VCircularQueue_Size(cqueue));
	EXPECT_ASSERTION(VCircularQueue_Push(cqueue, &element5), ZCQUEUE_ERROR_PUSH_NO_SPACE_LEFT);
	Element element;
	EXPECT_NO_ASSERTIONS(VCircularQueue_Pop(cqueue, &element, sizeof(element)));
	TEST_REQUIRE(element == element1);
	TEST_REQUIRE(cqueue->start == 1);
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(4 == VCircularQueue_Size(cqueue));
	EXPECT_NO_ASSERTIONS(VCircularQueue_Pop(cqueue, &element, sizeof(element)));
	TEST_REQUIRE(element == element2);
	TEST_REQUIRE(cqueue->start == 2);
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(3 == VCircularQueue_Size(cqueue));
	EXPECT_NO_ASSERTIONS(VCircularQueue_Pop(cqueue, &element, sizeof(element)));
	TEST_REQUIRE(element == element3);
	TEST_REQUIRE(cqueue->start == 3);
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(2 == VCircularQueue_Size(cqueue));
	EXPECT_NO_ASSERTIONS(VCircularQueue_Pop(cqueue, &element, sizeof(element)));
	TEST_REQUIRE(element == element4);
	TEST_REQUIRE(cqueue->start == 4);
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(1 == VCircularQueue_Size(cqueue));
	EXPECT_NO_ASSERTIONS(VCircularQueue_Pop(cqueue, &element, sizeof(element)));
	TEST_REQUIRE(element == element5);
	TEST_REQUIRE(cqueue->start == 0);
	TEST_REQUIRE(cqueue->end == 0);
	TEST_REQUIRE(0 == VCircularQueue_Size(cqueue));
	return 0;
}
//............................................................................
int main() {
	utest_init();
	utest_enableDebugPrint();
	test_lembuff_ctor_invalid();
	test_lembuff_valid();
	test_vcqueue_valid();
	return 0;
}
